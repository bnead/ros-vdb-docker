FROM ros:noetic-perception

RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y autoremove && \
    apt-get -y autoclean && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get -y install \
    build-essential \
    cmake \
    git \
    libboost-iostreams-dev \
    libboost-system-dev \
    libjemalloc-dev \
    libtbb-dev \
    libilmbase-dev \
    libopenexr-dev \
    zlib1g-dev \
    python3-catkin-tools \
    python3-osrf-pycommon \
    ros-noetic-tf-conversions

# c-blosc
RUN git clone https://github.com/Blosc/c-blosc.git -b v1.5.0 && \
    mkdir c-blosc/build && \
    cd c-blosc/build && \
    cmake .. && make install -j8 && \
    cd ../../ && \
    rm -rf c-blosc

# VDB
RUN git clone https://github.com/AcademySoftwareFoundation/openvdb.git && \
    mkdir openvdb/build && \
    cd openvdb/build && \
    cmake .. && make install -j8 && \
    cd ../../ && \
    rm -rf openvdb
